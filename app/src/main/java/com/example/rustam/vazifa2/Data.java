package com.example.rustam.vazifa2;

/**
 * Created by Rustam on 23.02.2017.
 */

public class Data {
    boolean type;
    String item;
    String section;
    String item2;

    public Data(boolean type, String item,String section,String item2) {
        this.type = type;
        this.item = item;
         this.section=section;
        this.item2=item2;
    }

    public boolean isType() {
        return type;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
