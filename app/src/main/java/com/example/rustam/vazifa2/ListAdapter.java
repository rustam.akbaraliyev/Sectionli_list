package com.example.rustam.vazifa2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Rustam on 23.02.2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<Data> MyData;
    private Data base;
    private Context context;
    public static final int ITEM_TYPE_Item = 0;
    public static final int ITEM_TYPE_Section = 1;

    public ListAdapter(List<Data> myData,Context con) {
        MyData = myData;
        context=con;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View headerRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_item,parent,false);
        headerRow.setBackgroundResource(R.drawable.color_section);
        View normalView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent,false);
        normalView.setBackgroundResource(R.drawable.color_items);
        if (viewType == ITEM_TYPE_Item) {

            return new ViewHolder(normalView); // view holder for normal items
        }
        else
            return new ViewHolder(headerRow); // view holder for header items

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
            base=MyData.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,""+position,Toast.LENGTH_SHORT).show();
            }
        });
        final int itemType = getItemViewType(position);

        if(itemType==ITEM_TYPE_Section)
        {
            holder.sections.setText(base.getSection());

        }
        else
        {
            holder.subtext.setText(base.getItem());
            holder.text2.setText(base.getItem2());
        }
        // holder.subtext.setText(base.getItem());
    }

    @Override
    public int getItemCount() {
        return MyData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView subtext;
        TextView sections;
        TextView text2;
        public ViewHolder(View itemView) {
            super(itemView);
            subtext= (TextView) itemView.findViewById(R.id.items);
            sections=(TextView) itemView.findViewById(R.id.section);
            text2=(TextView) itemView.findViewById(R.id.itemlar);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (MyData.get(position).isType()) {
            return ITEM_TYPE_Item;
        } else {
            return ITEM_TYPE_Section;
        }
    }
}
