package com.example.rustam.vazifa2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Data> Base;
    boolean[] sectionlar=new boolean[17];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sectionlar[0]=false;sectionlar[1]=true;sectionlar[2]=true;sectionlar[3]=true;sectionlar[4]=true;sectionlar[5]=true;
        sectionlar[6]=false;
        sectionlar[7]=true;
        sectionlar[8]=true;
        sectionlar[9]=false;
        sectionlar[10]=true;
        sectionlar[11]=true;
        sectionlar[12]=true;
        sectionlar[13]=true;
        sectionlar[14]=true;
        sectionlar[15]=true;
        sectionlar[16]=true;
        Base=load();
        RecyclerView recyclerView= (RecyclerView) findViewById(R.id.recycler);
        RecyclerView.LayoutManager manager=new GridLayoutManager(this,1);
        ListAdapter adapter=new ListAdapter(Base,MainActivity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    public List<Data> load()
    {
        List<Data> t= new ArrayList<Data>();
            for(int i=0; i<100; i++)
            {
                t.add(new Data(sectionlar[i%17],"Ism "+i,"FIO "+i,"Sharif "+i));
            }
        return t;
    }
}
